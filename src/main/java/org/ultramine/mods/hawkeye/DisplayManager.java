package org.ultramine.mods.hawkeye;

import java.text.SimpleDateFormat;
import java.util.List;

import net.minecraft.event.ClickEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;

import org.ultramine.mods.hawkeye.entry.DataEntry;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Manages displaying of search results. Includes utilities for handling pages
 * of results
 * 
 * @author oliverw92
 */
public class DisplayManager
{
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	
	/**
	 * Displays a page of data from the specified {@link PlayerSession} search
	 * results. Contains appropriate methods for detecing errors e.g. no results
	 * 
	 * @param session
	 *            {@link PlayerSession}
	 * @param page
	 *            page number to display
	 */
	public static void displayPage(PlayerSession session, int page)
	{
		//Check if any results are found
		List<DataEntry> results = session.getSearchResults();
		if(results == null || results.size() == 0)
		{
			HawkUtil.sendMessage(session.getSender(), "&cNo results found");
			return;
		}

		//Work out max pages. Return if page is higher than max pages
		int maxLines = session.isOnePoint() ? 10 : 8;
		int maxPages = (int) Math.ceil((double) results.size() / maxLines);
		if(page > maxPages || page < 1)
			return;

		//Begin displaying page
		HawkUtil.sendMessage(session.getSender(), "&8--------------------- &7Page (&c" + page + "&7/&c" + maxPages + "&7) &8--------------------" + (maxPages < 9 ? "-" : ""));
		if(session.isOnePoint())
		{
			ChatComponentText line = new ChatComponentText("Location: ");
			line.getChatStyle().setColor(EnumChatFormatting.GRAY);
			line.appendSibling(formatCoords(results.get(0)))
				.appendText(" Actions: ")
				.appendSibling(new ChatComponentText(Integer.toString(results.size())).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.YELLOW)));
			session.getSender().addChatMessage(line);
		}

		for(int i = (page - 1) * maxLines; i < ((page - 1) * maxLines) + maxLines; i++)
		{
			if(i == results.size())
				break;
			DataEntry entry = results.get(i);

//			sendLine(session.getSender(), "&cid:" + entry.getDataId() + " &7" + entry.getFormattedDate().substring(5) + " &c" + entry.getPlayer() + " &7" + entry.getType().getConfigName());
//			sendLine(session.getSender(), "   &cLoc: &7" + entry.getWorld() + "-" + entry.getX() + "," + entry.getY() + "," + entry.getZ() + " &cData: &7" + entry.getStringData());
			if(session.isOnePoint())
			{
				session.getSender().addChatMessage(
					formatDate(entry)
					.appendText(" ")
					.appendSibling(formatPlayer(entry))
					.appendText(" ")
					.appendSibling(formatAction(entry))
					.appendText(" ")
					.appendSibling(formatData(entry))
				);
			}
			else
			{
				ChatComponentText line1 = new ChatComponentText("");
				line1.getChatStyle().setColor(EnumChatFormatting.GRAY);
				line1.appendSibling(new ChatComponentText("| ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.DARK_GRAY)))
					.appendText("id:")
					.appendSibling(formatID(entry))
					.appendText(" ")
					.appendSibling(formatDate(entry))
					.appendText(" ")
					.appendSibling(formatPlayer(entry))
					.appendText(" ")
					.appendSibling(formatAction(entry));
				session.getSender().addChatMessage(line1);

				ChatComponentText line2 = new ChatComponentText("");
				line2.getChatStyle().setColor(EnumChatFormatting.GRAY);
				line2.appendSibling(new ChatComponentText("| ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.DARK_GRAY)))
					.appendText("   Loc: ")
					.appendSibling(formatCoords(entry))
					.appendText(" Data: ")
					.appendSibling(formatData(entry));
				session.getSender().addChatMessage(line2);
			}
		}
//		Util.sendMessage(session.getSender(), "&8-----------------------------------------------------");
		return;
	}
	
	private static IChatComponent withColor(EnumChatFormatting color, String str)
	{
		return new ChatComponentText(str).setChatStyle(new ChatStyle().setColor(color));
	}
	
	private static IChatComponent formatID(DataEntry entry)
	{
		String sid = Long.toString(entry.getDataId());
		ChatComponentText id = new ChatComponentText(sid);
		id.getChatStyle().setColor(EnumChatFormatting.RED);
		id.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, sid));
		return id;
	}
	
	private static IChatComponent formatDate(DataEntry entry)
	{
		return withColor(EnumChatFormatting.DARK_AQUA, DATE_FORMAT.format(entry.getDate()));
	}
	
	private static IChatComponent formatPlayer(DataEntry entry)
	{
		ChatComponentText player = new ChatComponentText(entry.getPlayer());
		player.getChatStyle().setColor(EnumChatFormatting.RED);
		player.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, entry.getPlayer()));
		return player;
	}
	
	private static IChatComponent formatAction(DataEntry entry)
	{
		return withColor(EnumChatFormatting.GOLD, entry.getType().getConfigName());
	}
	
	private static IChatComponent formatCoords(DataEntry entry)
	{
		ChatComponentText coords = new ChatComponentText("["+entry.getWorld()+"]("+entry.getX()+","+entry.getY()+","+entry.getZ()+")");
		coords.getChatStyle().setColor(EnumChatFormatting.GOLD);
		coords.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/tp " + entry.getWorld() + " " + entry.getX() + " " + entry.getY() + " " + entry.getZ()));
		return coords;
	}
	
	private static IChatComponent formatData(DataEntry entry)
	{
		IChatComponent data = entry.getStringData();
		if(data.getChatStyle().getColor() == null)
			data.getChatStyle().setColor(EnumChatFormatting.GRAY);
		return data;
	}
}
