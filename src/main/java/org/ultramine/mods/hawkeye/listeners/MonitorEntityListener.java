package org.ultramine.mods.hawkeye.listeners;

import org.ultramine.mods.hawkeye.DataType;
import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.mods.hawkeye.database.DataManager;
import org.ultramine.mods.hawkeye.entry.DataEntry;
import org.ultramine.mods.hawkeye.util.HawkNameUtil;
import org.ultramine.server.event.HangingEvent.HangingBreakEvent;

import com.mojang.authlib.GameProfile;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.item.EntityPainting;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

/**
 * Entity listener class for HawkEye Contains system for managing player deaths
 * 
 * @author oliverw92
 */
public class MonitorEntityListener
{
	//@HawkEvent(dataType = {DataType.PVP_DEATH, DataType.MOB_DEATH, DataType.OTHER_DEATH, DataType.ENTITY_KILL})
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onEntityDeath(LivingDeathEvent event)
	{
		EntityLivingBase entity = event.entityLiving;
		Entity damager = event.source.getEntity();

		if(entity.isEntityPlayer())
		{//Player death
			EntityPlayer victim = (EntityPlayer) entity;
			HawkEye.containerManager.checkInventoryClose(victim);

			//Mob or PVP death
			if(damager != null)
			{
				if(damager.isEntityPlayer())
				{
					DataManager.addEntry(new DataEntry(victim, DataType.PVP_DEATH, victim.worldObj, victim.posX, victim.posY, victim.posZ, HawkNameUtil.getEntityOwnerOrName(damager)));
				}
				else
				{
					DataManager.addEntry(new DataEntry(victim, DataType.MOB_DEATH, victim.worldObj, victim.posX, victim.posY, victim.posZ, HawkNameUtil.getEntityOwnerOrName(damager)));
				}
			}
			else
			{//Other death
				DataManager.addEntry(new DataEntry(victim, DataType.OTHER_DEATH, victim.worldObj, victim.posX, victim.posY, victim.posZ, event.source.getDamageType()));
			}

			//Log item drops
			if(HawkEye.instance.config.general.logDeathDrops && !victim.worldObj.getGameRules().getGameRuleBooleanValue("keepInventory"))
			{
				for(int i = 0; i < victim.inventory.getSizeInventory(); i++)
				{
					ItemStack is = victim.inventory.getStackInSlot(i);
					if(is != null)
					{
						String data;
						if(is.getItemDamage() != 0)
						{
							data = is.stackSize + "x " + Item.getIdFromItem(is.getItem()) + ":" + is.getItemDamage();
						}
						else
						{
							data = is.stackSize + "x " + Item.getIdFromItem(is.getItem());
						}

						DataManager.addEntry(new DataEntry(victim, DataType.ITEM_DROP, victim.worldObj, victim.posX, victim.posY, victim.posZ, data));
					}
				}
			}
		}
		else
		{//Mob Death
			if(damager != null && damager.isEntityPlayer())
			{
				DataManager.addEntry(new DataEntry((EntityPlayer)damager, DataType.ENTITY_KILL, entity.worldObj, entity.posX, entity.posY, entity.posZ, HawkNameUtil.getEntityOwnerOrName(entity)));
			}
		}
	}

	//@HawkEvent(dataType = DataType.PAINTING_BREAK)
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onHangingBreak(HangingBreakEvent e)
	{
		GameProfile owner = e.entity.worldObj.getEventProxy().getObjectOwner();
		String who = owner == null ? null : owner.getName();
		if(who == null)
			who = HawkNameUtil.ENVIRONMENT;

		String name = "";
		if(e.entity instanceof EntityPainting)
		{
			name = "65";
		}
		else if(e.entity instanceof EntityItemFrame)
		{
			ItemStack display = ((EntityItemFrame) e.entity).getDisplayedItem();
			if(display != null)
			{
				name = "133+" + Item.getIdFromItem(display.getItem()) + ":" + display.getItemDamage();
			}
			else
			{
				name = "133";
			}
		}
		DataManager.addEntry(new DataEntry(who, DataType.HANGING_BREAK, e.entity.worldObj, e.entity.posX, e.entity.posY, e.entity.posZ, name));
	}
//
//	//@HawkEvent(dataType = DataType.PAINTING_PLACE)
//	@ForgeSubscribe(priority = EventPriority.LOWEST)
//	public void onPaintingPlace(HangingPlaceEvent event)
//	{
//		String who = "Environment";
//		EntityPlayer player = event.getPlayer();
//		if(player != null)
//		{
//			who = player.username;
//		}
//
//		String name = "";
//		if(event.entity instanceof EntityPainting)
//		{
//			name = "65";
//		}
//		else if(event.entity instanceof EntityItemFrame)
//		{
//			name = "133";
//		}
//		DataManager.addEntry(new DataEntry(who, DataType.PAINTING_PLACE, event.entity.worldObj, event.entity.xPosition, event.entity.yPosition, event.entity.zPosition, name));
//	}
}
