package org.ultramine.mods.hawkeye.listeners;

import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.mods.hawkeye.SessionManager;
import org.ultramine.mods.hawkeye.ToolManager;
import org.ultramine.mods.hawkeye.util.HawkBlockUtil;
import org.ultramine.mods.hawkeye.util.HawkVector;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent;

/**
 * Block listener class for HawkEye Tools
 * 
 * @author oliverw92
 */
public class ToolListener
{
//	@SubscribeEvent(priority = EventPriority.HIGH)
//	public void onBlockPlace(BlockEvent.PlaceEvent e)
//	{
//		if(HawkBlockUtil.getItemString(e.itemInHand).equals(HawkEye.instance.config.general.toolBlock) && SessionManager.getSession(e.player).isUsingTool())
//		{
//			ToolManager.toolSearch(e.player, new HawkVector(e.x, e.y, e.z));
//			e.setCanceled(true);
//		}
//	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		EntityPlayer player = event.entityPlayer;
		if(event.action == Action.RIGHT_CLICK_BLOCK && player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().getItem() == Items.wooden_pickaxe)
		{
			ToolManager.toolSearch(player, new HawkVector(event.x, event.y, event.z));
			event.setCanceled(true);
		}
		else if(event.action == Action.LEFT_CLICK_BLOCK && player.inventory.getCurrentItem() != null
				&& HawkBlockUtil.getItemString(player.inventory.getCurrentItem()).equals(HawkEye.instance.config.general.toolBlock) && SessionManager.getSession(player).isUsingTool())
		{
			ToolManager.toolSearch(player, new HawkVector(event.x, event.y, event.z));
			event.setCanceled(true);
		}
	}

}
