package org.ultramine.mods.hawkeye.listeners;

import org.ultramine.mods.hawkeye.DataType;
import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.mods.hawkeye.database.DataManager;
import org.ultramine.mods.hawkeye.entry.DataEntry;
import org.ultramine.mods.hawkeye.util.HawkUtil;
import org.ultramine.server.event.InventoryCloseEvent;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

/**
 * Player listener class for HawkEye
 * 
 * @author oliverw92
 */
public class MonitorPlayerListener
{
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerLogin(PlayerLoggedInEvent e)
	{
		DataManager.addEntry(new DataEntry(e.player, DataType.JOIN, e.player.worldObj, e.player.posX, e.player.posY, e.player.posZ,
				HawkEye.instance.config.general.logIpAddresses ? ((EntityPlayerMP) e.player).playerNetServerHandler.netManager.getSocketAddress().toString().substring(1) : ""));
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerLogout(PlayerLoggedOutEvent e)
	{
		if(e.player != null && ((EntityPlayerMP) e.player).playerNetServerHandler != null)
		{
			HawkEye.containerManager.checkInventoryClose(e.player);

			DataManager.addEntry(new DataEntry(e.player, DataType.QUIT, e.player.worldObj, e.player.posX, e.player.posY, e.player.posZ,
					HawkEye.instance.config.general.logIpAddresses ? ((EntityPlayerMP) e.player).playerNetServerHandler.netManager.getSocketAddress().toString().substring(1) : ""));
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerChangedDimension(PlayerChangedDimensionEvent e)
	{
		HawkEye.containerManager.checkInventoryClose(e.player);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerRespawn(PlayerRespawnEvent e)
	{
		HawkEye.containerManager.checkInventoryClose(e.player);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerChat(ServerChatEvent e)
	{
		EntityPlayer player = e.player;
		if(player != null)
			DataManager.addEntry(new DataEntry(player, DataType.CHAT, player.worldObj, player.posX, player.posY, player.posZ, e.message));
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerCommandPreprocess(CommandEvent e)
	{
		if(e.sender instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)e.sender;
			if(HawkEye.instance.config.commandFilter.contains(e.command.getCommandName()))
				return;
			DataManager.addEntry(new DataEntry(player, DataType.COMMAND, player.worldObj, player.posX, player.posY, player.posZ, e.command.getCommandName() + " "
					+ HawkUtil.join(e.parameters, " ")));
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerDropItem(ItemTossEvent e)
	{
		ItemStack is = e.entityItem.getEntityItem();
		if(is != null)
		{
			String data;
			if(is.getItemDamage() != 0)
			{
				data = is.stackSize + "x " + Item.getIdFromItem(is.getItem()) + ":" + is.getItemDamage();
			}
			else
			{
				data = is.stackSize + "x " + Item.getIdFromItem(is.getItem());
			}

			DataManager.addEntry(new DataEntry(e.player, DataType.ITEM_DROP, e.entityItem.worldObj, e.entityItem.posX, e.entityItem.posY,
					e.entityItem.posZ, data));
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerPickupItem(EntityItemPickupEvent e)
	{
		ItemStack is = e.item.getEntityItem();
		if(is != null)
		{
			String data;
			if(is.getItemDamage() != 0)
			{
				data = is.stackSize + "x " + Item.getIdFromItem(is.getItem()) + ":" + is.getItemDamage();
			}
			else
			{
				data = is.stackSize + "x " + Item.getIdFromItem(is.getItem());
			}

			DataManager.addEntry(new DataEntry(e.entityPlayer, DataType.ITEM_PICKUP, e.entityPlayer.worldObj, e.item.posX, e.item.posY,
					e.item.posZ, data));
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onInventoryClose(InventoryCloseEvent e)
	{
		HawkEye.containerManager.checkInventoryClose(e.player);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerInteract(PlayerInteractEvent e)
	{
		if(e.useBlock != Result.DENY && e.action == Action.RIGHT_CLICK_BLOCK)
		{
			EntityPlayer player = e.entityPlayer;
			Block block = player.worldObj.getBlock(e.x, e.y, e.z);
			if(block == Blocks.air)
				return;
			int id = Block.getIdFromBlock(block);
			TileEntity te = player.worldObj.getTileEntity(e.x, e.y, e.z);

			HawkEye.containerManager.checkInventoryClose(player);

			if(te instanceof IInventory)
			{
				HawkEye.containerManager.checkInventoryOpen(player, (IInventory)te, e.world, e.x, e.y, e.z);
				int meta = player.worldObj.getBlockMetadata(e.x, e.y, e.z);
				String blockStr = meta == 0 ? Integer.toString(id) : Integer.toString(id) + ":" + Integer.toString(meta);
				DataManager.addEntry(new DataEntry(player, DataType.BLOCK_INTERACT, player.worldObj, e.x, e.y, e.z, blockStr));
				return;
			}

			switch(id)
			{
			case 64: //WOODEN_DOOR
			case 96: //TRAP_DOOR
			case 107: //FENCE_GATE
			case 69: //lever
			case 77: //STONE_BUTTON
			case 143: //WOODEN_BUTTON
				DataManager.addEntry(new DataEntry(player, DataType.BLOCK_INTERACT, player.worldObj, e.x, e.y, e.z, ""));
				break;
			}

		}

	}
	
//	@SubscribeEvent(priority = EventPriority.LOWEST)
//	public void onPlayerInteractWithEntity(EntityInteractEvent e)
//	{
//		if(e.target instanceof IInventory)
//		{
//			HawkEye.containerManager.checkInventoryOpen(e.entityPlayer, (IInventory)e.target, e.entityPlayer.worldObj, e.target.posX, e.target.posY, e.target.posZ);
//		}
//	}

	/*
		public MonitorPlayerListener(HawkEye HawkEye) {
			super(HawkEye);
		}

		

		

		@HawkEvent(dataType = DataType.TELEPORT)
		public void onPlayerTeleport(PlayerTeleportEvent e) {
			//Check for inventory close
			HawkEye.containerManager.checkInventoryClose(e.getPlayer());
			Location from = e.getFrom();
			Location to   = e.getTo();
			if (Util.distance(from, to) > 5)
				DataManager.addEntry(new DataEntry(e.getPlayer(), DataType.TELEPORT, from, to.getWorld().getName() + ": " + to.getX() + ", " + to.getY() + ", " + to.getZ()));
		}

		@HawkEvent(dataType = {DataType.OPEN_CONTAINER, DataType.DOOR_INTERACT, DataType.LEVER, DataType.STONE_BUTTON, DataType.LAVA_BUCKET, DataType.WATER_BUCKET})
		public void onPlayerInteract(PlayerInteractEvent e) {
			Player player = e.getPlayer();
			Block block = e.getClickedBlock();

			//Check for inventory close
			HawkEye.containerManager.checkInventoryClose(player);

			if (block != null) {

				Location loc = block.getLocation();

				switch (block.getType()) {
					case FURNACE:
					case DISPENSER:
					case CHEST:
						if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
							//Call container manager for inventory open
							HawkEye.containerManager.checkInventoryOpen(player, block);
							DataManager.addEntry(new DataEntry(player, DataType.OPEN_CONTAINER, loc, Integer.toString(block.getTypeId())));
						}
						break;
					case WOODEN_DOOR:
					case TRAP_DOOR:
					case FENCE_GATE:
						DataManager.addEntry(new DataEntry(player, DataType.DOOR_INTERACT, loc, ""));
						break;
					case LEVER:
						DataManager.addEntry(new DataEntry(player, DataType.LEVER, loc, ""));
						break;
					case STONE_BUTTON:
						DataManager.addEntry(new DataEntry(player, DataType.STONE_BUTTON, loc, ""));
						break;
				}

				if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					loc = block.getRelative(e.getBlockFace()).getLocation();
					switch (player.getItemInHand().getType()) {
						case FLINT_AND_STEEL:
							DataManager.addEntry(new SimpleRollbackEntry(player, DataType.FLINT_AND_STEEL, loc, ""));
							break;
						case LAVA_BUCKET:
							DataManager.addEntry(new SimpleRollbackEntry(player, DataType.LAVA_BUCKET, loc, ""));
							break;
						case WATER_BUCKET:
							DataManager.addEntry(new SimpleRollbackEntry(player, DataType.WATER_BUCKET, loc, ""));
							break;
					}
				}

			}

		}
	*/
}
