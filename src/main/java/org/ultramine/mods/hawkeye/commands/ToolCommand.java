package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.ToolManager;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Enables or disables search tool for players
 * 
 * @author oliverw92
 */
public class ToolCommand extends BaseCommand
{
	public ToolCommand()
	{
		name = "tool";
		argLength = 0;
		usage = " <- enables/disables the searching tool";
	}

	@Override
	public boolean execute()
	{
		//If not using tool, enable
		if(!session.isUsingTool())
			ToolManager.enableTool(session, player);

		//If using tool, disable
		else
			ToolManager.disableTool(session, player);

		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cGives you the HawkEye tool. You can use this to see changes at specific places");
		HawkUtil.sendMessage(sender, "&cLeft click a block or place the tool to get information");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.tool(sender);
	}
}
