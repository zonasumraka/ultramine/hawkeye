package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.DisplayManager;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Displays a page from the player's previous search results
 * 
 * @author oliverw92
 */
public class PageCommand extends BaseCommand
{
	public PageCommand()
	{
		bePlayer = false;
		name = "page";
		argLength = 1;
		usage = "<page> <- display a page from your last search";
	}

	@Override
	public boolean execute()
	{
		DisplayManager.displayPage(session, Integer.parseInt(args.get(0)));
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cShows the specified page of results from your latest search");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.page(sender);
	}
}