package org.ultramine.mods.hawkeye.callbacks;

import net.minecraft.command.ICommandSender;

import org.ultramine.mods.hawkeye.PlayerSession;
import org.ultramine.mods.hawkeye.Rebuild;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchError;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Implementation of BaseCallback for use in rollback commands
 * 
 * @author oliverw92
 */
public class RebuildCallback extends BaseCallback
{
	private final PlayerSession session;
	private final ICommandSender sender;

	public RebuildCallback(PlayerSession session)
	{
		this.session = session;
		sender = session.getSender();
		HawkUtil.sendMessage(sender, "&cSearching for matching results to rebuild...");
	}

	@Override
	public void execute()
	{
		session.setRollbackResults(results);
		new Rebuild(session);
	}

	@Override
	public void error(SearchError error, String message)
	{
		HawkUtil.sendMessage(session.getSender(), message);
	}
}
