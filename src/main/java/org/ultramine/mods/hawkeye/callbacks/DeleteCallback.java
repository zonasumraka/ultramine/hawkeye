package org.ultramine.mods.hawkeye.callbacks;

import net.minecraft.command.ICommandSender;

import org.ultramine.mods.hawkeye.PlayerSession;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchError;
import org.ultramine.mods.hawkeye.util.HawkUtil;

public class DeleteCallback extends BaseCallback
{
	private final ICommandSender sender;
	public int deleted;

	public DeleteCallback(PlayerSession session)
	{
		sender = session.getSender();
		HawkUtil.sendMessage(sender, "&cDeleting matching results...");
	}

	@Override
	public void execute()
	{
		HawkUtil.sendMessage(sender, "&c" + deleted + " entries removed from database.");
	}

	@Override
	public void error(SearchError error, String message)
	{
		HawkUtil.sendMessage(sender, message);
	}
}
