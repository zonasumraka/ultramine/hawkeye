package org.ultramine.mods.hawkeye;

import java.util.HashMap;

import net.minecraft.command.ICommandSender;

/**
 * Class for parsing managing player's {@PlayerSession}s
 * 
 * @author oliverw92
 */
public class SessionManager
{
	private static final HashMap<String, PlayerSession> playerSessions = new HashMap<String, PlayerSession>();

	public SessionManager()
	{

	}

	/**
	 * Get a PlayerSession from the list
	 */
	public static PlayerSession getSession(ICommandSender player)
	{
		PlayerSession session = playerSessions.get(player.getCommandSenderName().toLowerCase());
		if(session == null)
			session = addSession(player);
		session.setSender(player);
		return session;
	}

	/**
	 * Adds a PlayerSession to the list
	 */
	public static PlayerSession addSession(ICommandSender player)
	{
		String name = player.getCommandSenderName().toLowerCase();
		PlayerSession session;
		if(playerSessions.containsKey(name))
		{
			session = playerSessions.get(name);
			session.setSender(player);
		}
		else
		{
			session = new PlayerSession(player);
			playerSessions.put(name, session);
		}
		return session;
	}
}
