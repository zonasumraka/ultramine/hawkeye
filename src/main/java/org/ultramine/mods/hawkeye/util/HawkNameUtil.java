package org.ultramine.mods.hawkeye.util;

import com.mojang.authlib.GameProfile;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class HawkNameUtil
{
	public static final String ENVIRONMENT = "[environment]";
	public static final String UNKNOWN = "[unknown]";
	
	public static String getEntityOwnerOrName(Entity entity)
	{
		if(entity.isEntityPlayerMP())
		{
			return ((EntityPlayer)entity).getGameProfile().getName().toLowerCase();
		}
		else
		{
			if(entity instanceof EntityCreeper)
			{
				EntityLivingBase to = ((EntityCreeper)entity).getAttackTarget();
				if(to != null && to.isEntityPlayerMP())
				{
					return ((EntityPlayer) to).getGameProfile().getName();
				}
				else
				{
					to = ((EntityCreeper)entity).getAITarget();
					if(to != null && to.isEntityPlayerMP())
					{
						return ((EntityPlayer) to).getGameProfile().getName();
					}
				}
			}
			GameProfile owner = entity.getObjectOwner();
			return owner != null ? owner.getName() : "[e:" + EntityList.getEntityString(entity) + "]";
		}
	}
	
	public static String getTileEntityOwnerOrName(TileEntity tile)
	{
		GameProfile owner = tile.getObjectOwner();
		return owner != null ? owner.getName() : "[t:" + Block.blockRegistry.getNameForObject(tile.getBlockType()) + "]";
	}
	
	public static String getBlockOwnerName(Block block)
	{
		return "[b:" + Block.blockRegistry.getNameForObject(block) + "]";
	}
}
