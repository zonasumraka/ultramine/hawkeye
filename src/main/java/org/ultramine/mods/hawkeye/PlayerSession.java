package org.ultramine.mods.hawkeye;

import java.util.List;

import net.minecraft.command.ICommandSender;

import org.ultramine.mods.hawkeye.entry.DataEntry;

/**
 * Stores data specific to each player on the server. This class is persistent
 * over play quit and rejoins, but not over server reboots
 * 
 * @author oliverw92
 */
public class PlayerSession
{
	private ICommandSender sender;
	private List<DataEntry> searchResults = null;
	private List<DataEntry> rollbackResults = null;
	private boolean isOnePoint;
	private boolean usingTool = false;
	private boolean doingRollback = false;
	private String[] toolCommand = HawkEye.instance.config.general.defaultToolCommand;
	private boolean inPreview = false;

	public PlayerSession(ICommandSender sender)
	{
		this.sender = sender;
	}

	public ICommandSender getSender()
	{
		return sender;
	}

	public void setSender(ICommandSender sender)
	{
		this.sender = sender;
	}

	public List<DataEntry> getSearchResults()
	{
		return searchResults;
	}

	public void setSearchResults(List<DataEntry> searchResults, boolean isOnePoint)
	{
		this.searchResults = searchResults;
		this.isOnePoint = isOnePoint;
	}
	
	public void setSearchResults(List<DataEntry> searchResults)
	{
		setSearchResults(searchResults, false);
	}

	public List<DataEntry> getRollbackResults()
	{
		return rollbackResults;
	}
	
	public boolean isOnePoint()
	{
		return isOnePoint;
	}

	public void setRollbackResults(List<DataEntry> rollbackResults)
	{
		this.rollbackResults = rollbackResults;
	}

	public boolean isUsingTool()
	{
		return usingTool;
	}

	public void setUsingTool(boolean usingTool)
	{
		this.usingTool = usingTool;
	}

	public boolean doingRollback()
	{
		return doingRollback;
	}

	public void setDoingRollback(boolean doingRollback)
	{
		this.doingRollback = doingRollback;
	}

	public String[] getToolCommand()
	{
		return toolCommand;
	}

	public void setToolCommand(String[] toolCommand)
	{
		this.toolCommand = toolCommand;
	}

	public boolean isInPreview()
	{
		return inPreview;
	}

	public void setInPreview(boolean inPreview)
	{
		this.inPreview = inPreview;
	}

}
