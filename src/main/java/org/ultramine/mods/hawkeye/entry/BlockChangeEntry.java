package org.ultramine.mods.hawkeye.entry;

import org.ultramine.mods.hawkeye.DataType;
import org.ultramine.mods.hawkeye.util.HawkBlockUtil;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.server.S23PacketBlockChange;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.util.BlockSnapshot;

/**
 * Represents a block change entry - one block changing to another
 * 
 * @author oliverw92
 */
public class BlockChangeEntry extends DataEntry
{
	private String from = null;
	private String to = null;

	public BlockChangeEntry()
	{
	}

	public BlockChangeEntry(EntityPlayer player, DataType type, World world, double x, double y, double z, BlockSnapshot from, BlockSnapshot to)
	{
		setInfo(player, type, world, x, y, z);
		this.from = from.getReplacedBlock() == Blocks.air ? null : HawkBlockUtil.getBlockString(from);
		this.to = HawkBlockUtil.getBlockString(to);
	}

	public BlockChangeEntry(String player, DataType type, World world, double x, double y, double z, BlockSnapshot from, BlockSnapshot to)
	{
		setInfo(player, type, world, x, y, z);
		this.from = from.getReplacedBlock() == Blocks.air ? null : HawkBlockUtil.getBlockString(from);
		this.to = HawkBlockUtil.getBlockString(to);
	}

	public BlockChangeEntry(EntityPlayer player, DataType type, World world, double x, double y, double z, String from, String to)
	{
		setInfo(player, type, world, x, y, z);
		this.from = from;
		this.to = to;
	}

	public BlockChangeEntry(String player, DataType type, World world, double x, double y, double z, String from, String to)
	{
		setInfo(player, type, world, x, y, z);
		this.from = from;
		this.to = to;
	}

	@Override
	public IChatComponent getStringData()
	{
		IChatComponent toComp = HawkBlockUtil.getBlockComponent(to);
		toComp.getChatStyle().setColor(EnumChatFormatting.GREEN);
		if(from == null)
			return toComp;
		IChatComponent fromComp = HawkBlockUtil.getBlockComponent(from);
		fromComp.getChatStyle().setColor(EnumChatFormatting.RED);
		return new ChatComponentText("")
			.appendSibling(fromComp)
			.appendText(" \u21e8 ")
			.appendSibling(toComp);
	}

	@Override
	public String getSqlData()
	{
		return from == null ? to : from + "-" + to;
	}

	@Override
	public boolean rollback(World world)
	{
		if(from == null)
			world.setBlockSilently((int) getX(), (int) getY(), (int) getZ(), Blocks.air, 0, 3);
		else
			HawkBlockUtil.setBlockString(world, (int) getX(), (int) getY(), (int) getZ(), from);
		return true;
	}

	@Override
	public boolean rollbackPlayer(World world, EntityPlayerMP player1)
	{
		EntityPlayerMP player = (EntityPlayerMP) player1;

		S23PacketBlockChange p = new S23PacketBlockChange((int) getX(), (int) getY(), (int) getZ(), world);
		if(from == null)
		{
			p.field_148883_d = Blocks.air;
			p.field_148884_e = 0;
			player.playerNetServerHandler.sendPacket(p);
			return true;
		}
		else
		{
			Block block = Block.getBlockById(HawkBlockUtil.getIdFromString(from));
			if(block != null)
			{
				p.field_148883_d = block;
				p.field_148884_e = HawkBlockUtil.getDataFromString(from);
				player.playerNetServerHandler.sendPacket(p);
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean rebuild(World world)
	{
		if(to == null)
			return false;
		else
			HawkBlockUtil.setBlockString(world, (int) getX(), (int) getY(), (int) getZ(), to);
		return true;
	}

	@Override
	public void interpretSqlData(String data)
	{
		int splitInd = data.indexOf('-');
		if(splitInd == -1)
		{
			from = null;
			to = data;
		}
		else
		{
			from = data.substring(0, splitInd);
			to = data.substring(splitInd + 1);
			if(from.isEmpty())
				from = null;
		}
		if(to.isEmpty())
			to = null;
	}
}
