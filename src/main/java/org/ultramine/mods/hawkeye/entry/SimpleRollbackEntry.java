package org.ultramine.mods.hawkeye.entry;

import org.ultramine.mods.hawkeye.DataType;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.server.S23PacketBlockChange;
import net.minecraft.world.World;

/**
 * Used for simple rollbacks - sets the block to air regardless of the data
 * 
 * @author oliverw92
 */
public class SimpleRollbackEntry extends DataEntry
{
	public SimpleRollbackEntry()
	{
	}

	public SimpleRollbackEntry(EntityPlayer player, DataType type, World world, double x, double y, double z, String data)
	{
		setInfo(player, type, world, x, y, z);
		this.data = data;
	}

	public SimpleRollbackEntry(String player, DataType type, World world, double x, double y, double z, String data)
	{
		setInfo(player, type, world, x, y, z);
		this.data = data;
	}

	@Override
	public boolean rollback(World world)
	{
		world.setBlockSilently((int) getX(), (int) getY(), (int) getZ(), Blocks.air, 0, 3);
		return true;
	}

	@Override
	public boolean rollbackPlayer(World world, EntityPlayerMP player1)
	{
		EntityPlayerMP player = (EntityPlayerMP) player1;

		S23PacketBlockChange p = new S23PacketBlockChange((int) getX(), (int) getY(), (int) getZ(), world);
		p.field_148883_d = Blocks.air;
		p.field_148884_e = 0;
		player.playerNetServerHandler.sendPacket(p);

		return true;
	}
}
